package com.cursokotlin.dogedexproyect

import androidx.compose.ui.test.assertIsDisplayed
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onNodeWithTag
import androidx.compose.ui.test.performClick
import androidx.compose.ui.test.performTextInput
import com.cursokotlin.dogedexproyect.api.ApiResponseStatus
import com.cursokotlin.dogedexproyect.auth.AuthScreen
import com.cursokotlin.dogedexproyect.auth.AuthTasks
import com.cursokotlin.dogedexproyect.auth.AuthViewModel
import com.cursokotlin.dogedexproyect.model.User
import org.junit.Rule
import org.junit.Test

class AuthScreenTest {
    @get:Rule
    val composeTestRule = createComposeRule()

    @Test
    fun testTappingRegisterButtonOpenSignUpScreen() {

        class FakeAuthRepository: AuthTasks {
            override suspend fun signUp(
                email: String,
                password: String,
                passwordConfirm: String
            ): ApiResponseStatus<User> {
                TODO("Not yet implemented")
            }

            override suspend fun login(email: String, password: String): ApiResponseStatus<User> {
                TODO("Not yet implemented")
            }

        }

        val authViewModel = AuthViewModel(
            authRepository = FakeAuthRepository()
        )

        composeTestRule.setContent {
            AuthScreen(onUserLoggedIn = { }, authViewModel = authViewModel)
        }
        composeTestRule.onNodeWithTag(testTag = "login-screen-login-button").assertIsDisplayed()
        composeTestRule.onNodeWithTag(testTag = "login-screen-register-button").performClick()
        composeTestRule.onNodeWithTag(testTag = "sign-up-screen-sign-up-button").assertIsDisplayed()

    }

    @Test
    fun testEmailAndPasswordErrorShowsIfTappingLoginButtonAndNotEmail() {

        class FakeAuthRepository: AuthTasks {
            override suspend fun signUp(
                email: String,
                password: String,
                passwordConfirm: String
            ): ApiResponseStatus<User> {
                TODO("Not yet implemented")
            }

            override suspend fun login(email: String, password: String): ApiResponseStatus<User> {
                TODO("Not yet implemented")
            }

        }

        val authViewModel = AuthViewModel(
            authRepository = FakeAuthRepository()
        )

        composeTestRule.setContent {
            AuthScreen(onUserLoggedIn = { }, authViewModel = authViewModel)
        }
        composeTestRule.onNodeWithTag(testTag = "login-screen-login-button").performClick()
        composeTestRule.onNodeWithTag(useUnmergedTree = true, testTag = "error-field-email-login-screen").assertIsDisplayed()
        composeTestRule.onNodeWithTag(useUnmergedTree = true, testTag = "email-field-login-screen").performTextInput("test@gmail.com")
        composeTestRule.onNodeWithTag(testTag = "login-screen-login-button").performClick()
        composeTestRule.onNodeWithTag(useUnmergedTree = true, testTag = "error-field-password-login-screen").assertIsDisplayed()
    }
}
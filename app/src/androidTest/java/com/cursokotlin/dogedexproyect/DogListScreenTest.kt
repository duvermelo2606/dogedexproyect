package com.cursokotlin.dogedexproyect

import androidx.compose.ui.test.*
import androidx.compose.ui.test.junit4.createComposeRule
import com.cursokotlin.dogedexproyect.api.ApiResponseStatus
import com.cursokotlin.dogedexproyect.doglist.DogListScreen
import com.cursokotlin.dogedexproyect.doglist.DogListViewModel
import com.cursokotlin.dogedexproyect.doglist.DogTasks
import com.cursokotlin.dogedexproyect.model.Dog
import kotlinx.coroutines.flow.Flow
import org.junit.Rule
import org.junit.Test

class DogListScreenTest {
    @get:Rule
    val composeTestRule = createComposeRule()

    @Test
    fun progressBarShowsWhenLoadingState() {

        class FakeDogRepository : DogTasks {
            override suspend fun getDogCollection(): ApiResponseStatus<List<Dog>> {
                return ApiResponseStatus.Loading()
            }

            override suspend fun addDogToUser(dogId: Long): ApiResponseStatus<Any> {
                TODO("Not yet implemented")
            }

            override suspend fun getDogByMLId(mlDogId: String): ApiResponseStatus<Dog> {
                TODO("Not yet implemented")
            }

            override suspend fun getProbableDogs(probableDogsIds: ArrayList<String>): Flow<ApiResponseStatus<Dog>> {
                TODO("Not yet implemented")
            }

        }

        val viewModel = DogListViewModel(
            dogRepository = FakeDogRepository()
        )

        composeTestRule.setContent {
            DogListScreen(onDogClicked = {}, onNavigationIconClick = { }, viewModel = viewModel)
        }

        composeTestRule.onNodeWithTag(testTag = "loading-wheel").assertIsDisplayed()

    }

    @Test
    fun errorDialogShowsIfErrorGettingDogs() {

        class FakeDogRepository : DogTasks {
            override suspend fun getDogCollection(): ApiResponseStatus<List<Dog>> {
                return ApiResponseStatus.Error(messageId = R.string.there_was_an_error)
            }

            override suspend fun addDogToUser(dogId: Long): ApiResponseStatus<Any> {
                TODO("Not yet implemented")
            }

            override suspend fun getDogByMLId(mlDogId: String): ApiResponseStatus<Dog> {
                TODO("Not yet implemented")
            }

            override suspend fun getProbableDogs(probableDogsIds: ArrayList<String>): Flow<ApiResponseStatus<Dog>> {
                TODO("Not yet implemented")
            }

        }

        val viewModel = DogListViewModel(
            dogRepository = FakeDogRepository()
        )

        composeTestRule.setContent {
            DogListScreen(onDogClicked = {}, onNavigationIconClick = { }, viewModel = viewModel)
        }

        composeTestRule.onNodeWithTag(testTag = "error-dialog").assertIsDisplayed()

    }

    @Test
    fun dogListShowsGettingDogsIsSuccess() {

        class FakeDogRepository : DogTasks {
            override suspend fun getDogCollection(): ApiResponseStatus<List<Dog>> {
                return ApiResponseStatus.Success(
                    listOf(
                        Dog(1, 1, "Koby", "", "", "", "", "", "", "", "", true),
                        Dog(10 , 22, "Katy", "", "", "", "", "", "", "", "", false)
                    )
                )
            }

            override suspend fun addDogToUser(dogId: Long): ApiResponseStatus<Any> {
                TODO("Not yet implemented")
            }

            override suspend fun getDogByMLId(mlDogId: String): ApiResponseStatus<Dog> {
                TODO("Not yet implemented")
            }

            override suspend fun getProbableDogs(probableDogsIds: ArrayList<String>): Flow<ApiResponseStatus<Dog>> {
                TODO("Not yet implemented")
            }

        }

        val viewModel = DogListViewModel(
            dogRepository = FakeDogRepository()
        )

        composeTestRule.setContent {
            DogListScreen(onDogClicked = {}, onNavigationIconClick = { }, viewModel = viewModel)
        }

        composeTestRule.onNodeWithTag(useUnmergedTree = true, testTag = "dog-Koby").assertIsDisplayed()
        composeTestRule.onNodeWithText("22").assertIsDisplayed()
        //composeTestRule.onRoot(useUnmergedTree = true).printToLog("PRUEBA")
    }
}
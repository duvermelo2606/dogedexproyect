package com.cursokotlin.dogedexproyect

import androidx.compose.ui.test.*
import androidx.compose.ui.test.junit4.createAndroidComposeRule
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import com.cursokotlin.dogedexproyect.api.ApiResponseStatus
import com.cursokotlin.dogedexproyect.auth.AuthTasks
import com.cursokotlin.dogedexproyect.auth.LoginActivity
import com.cursokotlin.dogedexproyect.di.AuthTasksModule
import com.cursokotlin.dogedexproyect.model.User
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import dagger.hilt.android.testing.UninstallModules
import dagger.hilt.components.SingletonComponent
import org.junit.Rule
import org.junit.Test
import javax.inject.Inject

@HiltAndroidTest
@UninstallModules(AuthTasksModule::class)
class LoginActivityTest {
    @get:Rule(order = 0)
    var hiltRule = HiltAndroidRule(this)

    //@get:Rule(order = 1)
    //val runtimePermissionRule: GrantPermissionRule =
    //    GrantPermissionRule.grant(Manifest.permission.CAMERA)

    @get:Rule(order = 2)
    var composeTestRule = createAndroidComposeRule<LoginActivity>()

    class FakeAuthRepository @Inject constructor() : AuthTasks {
        override suspend fun signUp(
            email: String,
            password: String,
            passwordConfirm: String
        ): ApiResponseStatus<User> {
            TODO("Not yet implemented")
        }

        override suspend fun login(email: String, password: String): ApiResponseStatus<User> {
            return ApiResponseStatus.Success(
                User(1L, "duver@gmail.com", "tokenTest1234")
            )
        }

    }

    @Module
    @InstallIn(SingletonComponent::class)
    abstract class AuthTasksTestModule {
        @Binds
        abstract fun bindDogTasks(
            fakeAuthRepository: FakeAuthRepository
        ): AuthTasks
    }

    @Test
    fun mainActivityOpensAfterUSerLogin() {
        val context = composeTestRule.activity

        composeTestRule.onNodeWithText(context.getString(R.string.login)).assertIsDisplayed()
        composeTestRule.onNodeWithTag(useUnmergedTree = true, testTag = "email-field-login-screen")
            .performTextInput("duver@gmail.com")
        composeTestRule.onNodeWithTag(
            useUnmergedTree = true,
            testTag = "password-field-login-screen"
        ).performTextInput("duver1234")
        composeTestRule.onNodeWithText(context.getString(R.string.login)).performClick()

        onView(withId(R.id.btnFloatingTakePhoto)).check(matches(isDisplayed()))
    }


}
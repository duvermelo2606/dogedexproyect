package com.cursokotlin.dogedexproyect.doglist

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.recyclerview.widget.GridLayoutManager
import coil.annotation.ExperimentalCoilApi
import com.cursokotlin.dogedexproyect.api.ApiResponseStatus
import com.cursokotlin.dogedexproyect.databinding.ActivityDogListBinding
import com.cursokotlin.dogedexproyect.dogdetail.DogDetailComposeActivity
import com.cursokotlin.dogedexproyect.dogdetail.DogDetailComposeActivity.Companion.DOG_KEY
import com.cursokotlin.dogedexproyect.dogdetail.ui.theme.DogedexProyectTheme
import com.cursokotlin.dogedexproyect.model.Dog
import dagger.hilt.android.AndroidEntryPoint

//private const val GRID_SPAN_CONT = 3

@ExperimentalCoilApi
@AndroidEntryPoint
class DogListActivity : ComponentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {

            DogedexProyectTheme {


                DogListScreen(
                    onDogClicked = ::openDogDetailActivity,
                    onNavigationIconClick = ::onNavigationIconClick,
                )
            }
        }
    }

    private fun openDogDetailActivity(dog: Dog) {
        val intent = Intent(this, DogDetailComposeActivity::class.java)
        intent.putExtra(DogDetailComposeActivity.DOG_KEY, dog)
        startActivity(intent)
    }

    private fun onNavigationIconClick() {
        finish()
    }
    /*
    val binding = ActivityDogListBinding.inflate(layoutInflater)
    setContentView(binding.root)

    val pbLoadingDogList = binding.pbLoadingDogList

    val rvDog = binding.rvDog
    rvDog.layoutManager = GridLayoutManager(this, GRID_SPAN_CONT)

    val adapter = DogAdapter()
    adapter.setOnItemClickListener {
        // Se pasa el Dog a DogDetailActivity
        val intent = Intent(this, DogDetailComposeActivity::class.java)
        intent.putExtra(DOG_KEY, it)
        startActivity(intent)
    }

    rvDog.adapter = adapter

    dogListViewModel.dogList.observe(this) { dogList ->
        adapter.submitList(dogList)
    }

    dogListViewModel.status.observe(this) { status ->

        when (status) {
            is ApiResponseStatus.Error -> {
                pbLoadingDogList.visibility = View.GONE
                Toast.makeText(this, status.messageId, Toast.LENGTH_SHORT).show()
            }
            is ApiResponseStatus.Loading -> {
                pbLoadingDogList.visibility = View.VISIBLE
            }
            is ApiResponseStatus.Success -> {
                pbLoadingDogList.visibility = View.GONE
            }
        }

    }
    */
}

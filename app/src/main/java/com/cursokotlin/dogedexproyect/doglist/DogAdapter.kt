package com.cursokotlin.dogedexproyect.doglist

import android.view.LayoutInflater
import android.view.View
import android.view.View.OnLongClickListener
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.cursokotlin.dogedexproyect.R
import com.cursokotlin.dogedexproyect.model.Dog
import com.cursokotlin.dogedexproyect.databinding.DogListItemBinding

class DogAdapter : ListAdapter<Dog, DogAdapter.DogViewHolder>(DiffCallback) {

    companion object DiffCallback : DiffUtil.ItemCallback<Dog>() {
        override fun areItemsTheSame(oldItem: Dog, newItem: Dog): Boolean {
            return oldItem === newItem
        }

        override fun areContentsTheSame(oldItem: Dog, newItem: Dog): Boolean {
            return oldItem.id == newItem.id
        }
    }

    private var onItemClickListener: ((Dog) -> Unit)? = null
    fun setOnItemClickListener(onItemClickListener: (Dog) -> Unit) {
        this.onItemClickListener = onItemClickListener
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DogViewHolder {
        val binding = DogListItemBinding.inflate(LayoutInflater.from(parent.context))
        return DogViewHolder(binding)
    }

    override fun onBindViewHolder(dogViewHolder: DogViewHolder, position: Int) {
        val dog = getItem(position)
        dogViewHolder.bind(dog)
    }

    inner class DogViewHolder(private val binding: DogListItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(dog: Dog) {

            if (dog.inCollection) {
                binding.ivDogImage.visibility = View.VISIBLE
                binding.tvDogIndex.visibility = View.GONE

                binding.rlDogListItem.background = ContextCompat.getDrawable(
                    binding.ivDogImage.context,
                    R.drawable.dog_list_item_background
                )
                binding.rlDogListItem.setOnClickListener {
                    onItemClickListener?.invoke(dog)
                }

                binding.ivDogImage.load(dog.imageUrl)
            }else {
                binding.ivDogImage.visibility = View.GONE

                binding.tvDogIndex.visibility = View.VISIBLE
                binding.tvDogIndex.text = dog.index.toString()

                binding.rlDogListItem.background = ContextCompat.getDrawable(
                    binding.ivDogImage.context,
                    R.drawable.dog_list_item_null_background
                )

            }


        }
    }
}
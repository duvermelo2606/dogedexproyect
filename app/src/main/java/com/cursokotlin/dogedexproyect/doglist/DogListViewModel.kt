package com.cursokotlin.dogedexproyect.doglist

import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.cursokotlin.dogedexproyect.model.Dog
import com.cursokotlin.dogedexproyect.api.ApiResponseStatus
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class DogListViewModel @Inject constructor(
    private val dogRepository: DogTasks
) : ViewModel() {

    var dogList = mutableStateOf<List<Dog>>(listOf())
    private set

    var status = mutableStateOf<ApiResponseStatus<Any>?>(null)
    private set

    init {
        getDogCollection()
    }

    @Suppress("Unchecked_CAST")
    private fun handleResponseStatus(apiResponseStatus: ApiResponseStatus<List<Dog>>) {
        if (apiResponseStatus is ApiResponseStatus.Success) {
            dogList.value = apiResponseStatus.data
        }

        status.value = apiResponseStatus as ApiResponseStatus<Any>
    }

    private fun getDogCollection() {
        viewModelScope.launch {
            status.value = ApiResponseStatus.Loading()
            handleResponseStatus((dogRepository.getDogCollection()))
        }
    }

    fun resetApiResponseStatus() {
        status.value = null
    }
}
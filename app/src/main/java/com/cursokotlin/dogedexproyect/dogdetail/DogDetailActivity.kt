package com.cursokotlin.dogedexproyect.dogdetail

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.lifecycle.ViewModel
import coil.load
import com.cursokotlin.dogedexproyect.model.Dog
import com.cursokotlin.dogedexproyect.R
import com.cursokotlin.dogedexproyect.api.ApiResponseStatus
import com.cursokotlin.dogedexproyect.databinding.ActivityDogDetailBinding

class DogDetailActivity : AppCompatActivity() {

    companion object {
        const val DOG_KEY = "dog"
        const val IS_RECOGNITION_KEY = "is_recognition"
    }

    private val dogDetailViewModel: DogDetailViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        /*val binding = ActivityDogDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val dog = intent?.extras?.getParcelable<Dog>(DOG_KEY)
        val isRecognition = intent?.extras?.getBoolean(IS_RECOGNITION_KEY, false) ?: false

        if (dog == null) {
            Toast.makeText(this, R.string.error_showing_dog_not_found, Toast.LENGTH_SHORT).show()
            finish()
            return
        }

        binding.tvDogIndex.text = getString(R.string.dog_index_format, dog.index)
        binding.tvLifeExpectancy.text = getString(R.string.dog_life_expectancy_format, dog.lifeExpectancy)
        binding.dog = dog
        binding.dogImage.load(dog.imageUrl)

        binding.fbClose.setOnClickListener {
            if (isRecognition) dogDetailViewModel.addDogToUser(dog.id)
            else finish()
        }

         */
        /*
        dogDetailViewModel.status.observe(this) { status ->
            when(status) {
                is ApiResponseStatus.Error -> {
                    binding.pbLoadingWheel.visibility = View.GONE
                    Toast.makeText(this, status.messageId, Toast.LENGTH_SHORT).show()
                }
                is ApiResponseStatus.Loading -> binding.pbLoadingWheel.visibility = View.VISIBLE
                is ApiResponseStatus.Success -> {
                    binding.pbLoadingWheel.visibility = View.GONE
                    finish()
                }
            }
        }*/
    }
}
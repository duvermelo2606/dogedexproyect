package com.cursokotlin.dogedexproyect.dogdetail

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Check
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.semantics.semantics
import androidx.compose.ui.semantics.testTag
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.SavedStateHandle
import coil.annotation.ExperimentalCoilApi
import coil.compose.rememberAsyncImagePainter
import com.cursokotlin.dogedexproyect.R
import com.cursokotlin.dogedexproyect.api.ApiResponseStatus
import com.cursokotlin.dogedexproyect.composables.ErrorDialog
import com.cursokotlin.dogedexproyect.composables.LoadingWheel
import com.cursokotlin.dogedexproyect.dogdetail.DogDetailComposeActivity.Companion.DOG_KEY
import com.cursokotlin.dogedexproyect.doglist.DogTasks
import com.cursokotlin.dogedexproyect.model.Dog

@OptIn(ExperimentalCoilApi::class)
@Composable
fun DogDetailScreen(
    finishActivity: () -> Unit,
    detailViewModel: DogDetailViewModel = hiltViewModel()
) {
    val probableDogsDialogEnabled = remember { mutableStateOf(false) }
    val status = detailViewModel.status.value
    val dog = detailViewModel.dog.value
    val isRecognition = detailViewModel.isRecognition.value

    if (dog == null) {
        finishActivity()
        return
    }

    if (status is ApiResponseStatus.Success) {
        finishActivity()
    }

    Box(
        modifier = Modifier
            .fillMaxSize()
            .background(colorResource(id = R.color.secondary_background))
            .padding(start = 8.dp, end = 8.dp, bottom = 16.dp),
        contentAlignment = Alignment.TopCenter
    ) {
        DogInformation(dog, isRecognition = isRecognition) {
            detailViewModel.getProbableDogs()
            probableDogsDialogEnabled.value = true
        }
        Image(
            modifier = Modifier
                .width(270.dp)
                .padding(top = 80.dp),
            painter = rememberAsyncImagePainter(dog.imageUrl),
            contentDescription = dog.name
        )
        FloatingActionButton(
            onClick = {
                if (isRecognition) {
                    detailViewModel.addDogToUser()
                } else {
                    finishActivity()
                }
            },
            contentColor = Color.White,
            backgroundColor = colorResource(id = R.color.color_primary),
            modifier = Modifier
                .align(alignment = Alignment.BottomCenter)
                .semantics { testTag = "close-details-screen-fab" }
        ) {
            Icon(imageVector = Icons.Filled.Check, contentDescription = "")
        }

        if (status is ApiResponseStatus.Loading) {
            LoadingWheel()
        } else if (status is ApiResponseStatus.Error) {
            ErrorDialog(status.messageId) { detailViewModel.resetApiResponseStatus() }
        }

        val probableDogList = detailViewModel.probableDogList.collectAsState().value

        if (probableDogsDialogEnabled.value) {
            MostProbableDogsDialog(
                mostProbableDogs = probableDogList,
                onShowMostProbableDogsDialogDismiss = { probableDogsDialogEnabled.value = false },
                onItemClicked = { detailViewModel.updateDog(it) }
            )
        }
    }
}

@Composable
fun DogInformation(dog: Dog, isRecognition: Boolean, onProbableDogsButtonClick: () -> Unit) {
    Box(
        modifier = Modifier
            .fillMaxWidth()
            .padding(top = 180.dp)
    ) {
        Surface(
            modifier = Modifier.fillMaxWidth(),
            shape = RoundedCornerShape(4.dp),
            color = colorResource(id = android.R.color.white)
        ) {
            Column(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(8.dp),
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                Text(
                    modifier = Modifier.fillMaxWidth(),
                    text = stringResource(id = R.string.dog_index_format, dog.index),
                    fontSize = 32.sp,
                    color = colorResource(id = R.color.text_black),
                    textAlign = TextAlign.End
                )
                Text(
                    text = dog.name,
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(top = 32.dp, bottom = 8.dp, start = 8.dp, end = 8.dp),
                    color = colorResource(id = R.color.text_black),
                    textAlign = TextAlign.Center,
                    fontSize = 32.sp,
                    fontWeight = FontWeight.Medium
                )

                LifeIcon()

                Text(
                    stringResource(id = R.string.dog_life_expectancy_format, dog.lifeExpectancy),
                    textAlign = TextAlign.Center,
                    fontSize = 16.sp,
                    color = colorResource(id = R.color.text_black)
                )

                Text(
                    textAlign = TextAlign.Center,
                    text = dog.temperament,
                    fontSize = 16.sp,
                    color = colorResource(id = R.color.text_black),
                    fontWeight = FontWeight.Medium,
                    modifier = Modifier.padding(top = 8.dp)
                )

                if (isRecognition) {
                    Button(modifier = Modifier.padding(10.dp),
                        onClick = { onProbableDogsButtonClick() }
                    ) {
                        Text(
                            text = stringResource(id = R.string.not_your_dog_button),
                            textAlign = TextAlign.Center,
                            fontSize = 18.sp
                        )
                    }
                }

                Divider(
                    modifier = Modifier.padding(
                        top = 8.dp,
                        start = 8.dp,
                        end = 8.dp,
                        bottom = 16.dp
                    ),
                    color = colorResource(id = R.color.divider),
                    thickness = 1.dp
                )

                Row(
                    modifier = Modifier.fillMaxWidth(),
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    DogDataColumn(
                        modifier = Modifier.weight(1f),
                        stringResource(id = R.string.female),
                        dog.weightFemale,
                        dog.heightFemale
                    )

                    VerticalDivider()

                    Column(
                        modifier = Modifier.weight(1f),
                        horizontalAlignment = Alignment.CenterHorizontally
                    ) {
                        Text(
                            text = dog.type,
                            textAlign = TextAlign.Center,
                            fontSize = 16.sp,
                            color = colorResource(
                                id = R.color.text_black
                            ),
                            fontWeight = FontWeight.Medium,
                            modifier = Modifier.padding(top = 8.dp)
                        )
                        Text(
                            textAlign = TextAlign.Center,
                            text = stringResource(id = R.string.group),
                            fontSize = 16.sp,
                            color = colorResource(id = R.color.dark_gray),
                            modifier = Modifier.padding(top = 8.dp)
                        )
                    }

                    VerticalDivider()

                    DogDataColumn(
                        modifier = Modifier.weight(1f),
                        stringResource(id = R.string.male),
                        dog.weightMale,
                        dog.heightMale
                    )
                }

            }
        }
    }
}

@Composable
fun LifeIcon() {
    Row(
        verticalAlignment = Alignment.CenterVertically,
        modifier = Modifier
            .fillMaxWidth()
            .padding(start = 80.dp, end = 80.dp)
    ) {
        Surface(shape = CircleShape, color = colorResource(id = R.color.color_primary)) {
            Icon(
                painter = painterResource(id = R.drawable.ic_hearth_white),
                contentDescription = null,
                tint = Color.White,
                modifier = Modifier
                    .width(24.dp)
                    .height(24.dp)
                    .padding(4.dp)
            )
        }
        Surface(
            shape = RoundedCornerShape(bottomEnd = 2.dp, topEnd = 2.dp),
            modifier = Modifier
                .width(200.dp)
                .height(6.dp),
            color = colorResource(id = R.color.color_primary)
        ) {
        }
    }
}

@Composable
private fun VerticalDivider() {
    Divider(
        modifier = Modifier
            .height(42.dp)
            .width(1.dp), color = colorResource(id = R.color.divider)
    )
}

@Composable
private fun DogDataColumn(
    modifier: Modifier = Modifier,
    genre: String,
    weight: String,
    height: String
) {
    Column(
        modifier = modifier,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Text(
            text = genre,
            textAlign = TextAlign.Center,
            color = colorResource(id = R.color.text_black),
            modifier = Modifier.padding(top = 8.dp)
        )
        Text(
            text = weight,
            textAlign = TextAlign.Center,
            fontSize = 16.sp,
            color = colorResource(id = R.color.text_black),
            fontWeight = FontWeight.Medium,
            modifier = Modifier.padding(top = 8.dp)
        )
        Text(
            text = stringResource(id = R.string.weight),
            textAlign = TextAlign.Center,
            fontSize = 16.sp,
            color = colorResource(id = R.color.dark_gray)
        )
        Text(
            text = height,
            textAlign = TextAlign.Center,
            fontSize = 16.sp,
            color = colorResource(id = R.color.text_black),
            fontWeight = FontWeight.Medium,
            modifier = Modifier.padding(top = 8.dp)
        )
        Text(
            text = stringResource(id = R.string.height),
            textAlign = TextAlign.Center,
            fontSize = 16.sp,
            color = colorResource(id = R.color.dark_gray)
        )
    }
}

@ExperimentalCoilApi
@Preview
@Composable
fun DogDetailScreenPreview() {
    /*val fakeSavedStateHandle = SavedStateHandle()
    fakeSavedStateHandle[DOG_KEY] = Dog(
        1L, 78, "Pug", "Herding", "70", "75",
        "", "10 - 12", "Friendly, payful", "5", "6"
    )

    abstract class FakeDogRepository: DogTasks {
        override suspend fun getDogCollection(): ApiResponseStatus<List<Dog>> {
            TODO("Not yet implemented")
        }

        override suspend fun addDogToUser(dogId: Long): ApiResponseStatus<Any> {
            TODO("Not yet implemented")
        }

        override suspend fun getDogByMLId(mlDogId: String): ApiResponseStatus<Dog> {
            TODO("Not yet implemented")
        }

    }

    val fakeViewModel = DogDetailViewModel(
        dogRepository = FakeDogRepository(),
        savedStateHandle = fakeSavedStateHandle
    )

     */
    DogDetailScreen(
        finishActivity = {}
    )
}

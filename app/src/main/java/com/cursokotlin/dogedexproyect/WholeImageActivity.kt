package com.cursokotlin.dogedexproyect

import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import coil.load
import com.cursokotlin.dogedexproyect.databinding.ActivityWholeImageBinding
import java.io.File

class WholeImageActivity : AppCompatActivity() {

    private lateinit var binding: ActivityWholeImageBinding
    
    companion object {
        const val PHOTO_URI_KEY = "photo_uri"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityWholeImageBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val photoUri = intent.extras?.getString(PHOTO_URI_KEY)

        val uri = Uri.parse(photoUri)
        val path = uri.path
        
        if (path == null) {
            Toast.makeText(this, getString(R.string.error_showing_photo), Toast.LENGTH_SHORT).show()
            finish()
            return
        }

        binding.ivWholeImage.load(File(path))
    }
}
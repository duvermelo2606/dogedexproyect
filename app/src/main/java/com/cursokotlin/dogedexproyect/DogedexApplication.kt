package com.cursokotlin.dogedexproyect

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class DogedexApplication:Application()
package com.cursokotlin.dogedexproyect.composables

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material.OutlinedTextField
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.semantics.semantics
import androidx.compose.ui.semantics.testTag
import androidx.compose.ui.text.input.VisualTransformation

@Composable
fun AuthField(
    email: String,
    onTextChanged: (String) -> Unit,
    modifier: Modifier = Modifier,
    errorSemantic: String = "",
    label: String,
    fieldSemantic: String = "",
    visualTransformation: VisualTransformation = VisualTransformation.None,
    errorMessageId: Int? = null
) {
    Column(modifier = modifier) {
        if (errorMessageId != null) {
            Text(modifier = Modifier.fillMaxWidth().semantics { testTag = errorSemantic }, text = stringResource(id = errorMessageId))
        }
        OutlinedTextField(
            modifier = Modifier.fillMaxWidth()
                .semantics { testTag = fieldSemantic },
            value = email,
            onValueChange = { onTextChanged(it) },
            label = {
                Text(text = label)
            },
            visualTransformation = visualTransformation,
            isError = errorMessageId != null
        )
    }
}
package com.cursokotlin.dogedexproyect.di

import com.cursokotlin.dogedexproyect.auth.AuthRepository
import com.cursokotlin.dogedexproyect.auth.AuthTasks
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
abstract class AuthTasksModule {

    @Binds
    abstract fun bindAuthTasks(
        authRepository: AuthRepository
    ): AuthTasks
}
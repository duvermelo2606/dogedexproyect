package com.cursokotlin.dogedexproyect.di

import com.cursokotlin.dogedexproyect.machinelearning.ClassifierRepository
import com.cursokotlin.dogedexproyect.machinelearning.ClassifierTasks
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
abstract class ClassifierModule {
    @Binds
    abstract fun bindClassifierTasks(
        classifierRepository: ClassifierRepository
    ): ClassifierTasks
}
package com.cursokotlin.dogedexproyect.di

import com.cursokotlin.dogedexproyect.doglist.DogRepository
import com.cursokotlin.dogedexproyect.doglist.DogTasks
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
abstract class DogTasksModule {
    @Binds
    abstract fun bindDogTasks(
        dogRepository: DogRepository
    ): DogTasks
}
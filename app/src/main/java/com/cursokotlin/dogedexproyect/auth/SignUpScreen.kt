package com.cursokotlin.dogedexproyect.auth

import android.annotation.SuppressLint
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Button
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.semantics.semantics
import androidx.compose.ui.semantics.testTag
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import com.cursokotlin.dogedexproyect.R
import com.cursokotlin.dogedexproyect.composables.AuthField
import com.cursokotlin.dogedexproyect.composables.BackNavigationIcon

@SuppressLint("UnusedMaterialScaffoldPaddingParameter")
@Composable
fun SignUpScreen(
    onSignUpButtonClick: (email: String, password: String, confirmPassword:String) -> Unit,
    onNavigationIconClick: () -> Unit,
    resetFieldErrors: () -> Unit,
    authViewModel: AuthViewModel
) {

    Scaffold(
        topBar = { SignUpScreenTopBar(onNavigationIconClick) }
    ) {
        Content(onSignUpButtonClick = onSignUpButtonClick, authViewModel = authViewModel, resetFieldErrors  = resetFieldErrors)
    }
}

@Composable
private fun Content(
    onSignUpButtonClick: (email: String, password: String, confirmPassword:String) -> Unit,
    authViewModel: AuthViewModel,
    resetFieldErrors: () -> Unit
) {
    val email = remember { mutableStateOf("") }
    val password = remember { mutableStateOf("") }
    val confirmPassword = remember { mutableStateOf("") }

    Column(
        modifier = Modifier
            .fillMaxWidth()
            .padding(16.dp),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        AuthField(
            modifier = Modifier.fillMaxWidth(),
            email = email.value,
            onTextChanged = {
                email.value = it
                resetFieldErrors()
                            },
            label = stringResource(id = R.string.email),
            errorMessageId = authViewModel.emailError.value
        )
        AuthField(
            modifier = Modifier
                .fillMaxWidth()
                .padding(top = 16.dp),
            email = password.value,
            onTextChanged = {
                password.value = it
                resetFieldErrors()
                            },
            label = stringResource(id = R.string.password),
            visualTransformation = PasswordVisualTransformation(),
            errorMessageId = authViewModel.passwordError.value
        )
        AuthField(
            modifier = Modifier
                .fillMaxWidth()
                .padding(top = 16.dp),
            email = confirmPassword.value,
            onTextChanged = {
                confirmPassword.value = it
                resetFieldErrors()
                            },
            label = stringResource(id = R.string.password_confirm),
            visualTransformation = PasswordVisualTransformation(),
            errorMessageId = authViewModel.confirmPasswordError.value
        )

        Button(
            modifier = Modifier
                .fillMaxWidth()
                .padding(top = 16.dp)
                .semantics { testTag = "sign-up-screen-sign-up-button" },
            onClick = { onSignUpButtonClick(email.value, password.value, confirmPassword.value) }) {
            Text(
                text = stringResource(id = R.string.sign_up),
                textAlign = TextAlign.Center,
                fontWeight = FontWeight.Medium
            )
        }
    }
}

@Composable
fun SignUpScreenTopBar(onNavigationIconClick: () -> Unit) {
    TopAppBar(
        title = {
            Text(text = stringResource(id = R.string.app_name))
        },
        backgroundColor = Color.Red,
        contentColor = Color.White,
        navigationIcon = { BackNavigationIcon(onNavigationIconClick) }
    )
}

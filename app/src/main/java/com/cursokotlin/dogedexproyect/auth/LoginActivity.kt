package com.cursokotlin.dogedexproyect.auth

import android.content.Intent
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import coil.annotation.ExperimentalCoilApi
import com.cursokotlin.dogedexproyect.main.MainActivity
import com.cursokotlin.dogedexproyect.dogdetail.ui.theme.DogedexProyectTheme
import com.cursokotlin.dogedexproyect.model.User
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class LoginActivity : ComponentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContent {
            DogedexProyectTheme {
                AuthScreen(
                    onUserLoggedIn = ::startMainActivity,
                )
            }
        }

        /*val binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)

        viewModel.status.observe(this) { status ->
            when(status) {
                is ApiResponseStatus.Error -> {
                    binding.pbLoadingWheel.visibility = View.GONE
                    showErrorDialog(status.messageId)
                }
                is ApiResponseStatus.Loading -> binding.pbLoadingWheel.visibility = View.VISIBLE
                is ApiResponseStatus.Success -> binding.pbLoadingWheel.visibility = View.GONE
            }
        }

        viewModel.user.observe(this) {user ->
                if (user != null) {
                    User.setLoggedInUser(this, user)
                    startMainActivity()
                }
        }*/
    }

    /*
    override fun onRegisterButtonClick() {
        findNavController(R.id.fcvNavHost)
            .navigate(LoginFragmentDirections.actionLoginFragmentToSingUpFragment())
    }

    override fun onLoginFieldsValidated(email: String, password: String) {
        viewModel.login(email, password)
    }

    override fun onSignUpFieldsValidated(
        email: String,
        password: String,
        passwordConfirmation: String
    ) {
        viewModel.signUp(email, password, passwordConfirmation)
    }

    private fun showErrorDialog(messageId: Int) {
        AlertDialog.Builder(this)
            .setTitle(R.string.there_was_an_error)
            .setMessage(messageId)
            .setPositiveButton(android.R.string.ok) { _, _ ->

            }
            .create()
            .show()
    }

 */

    @OptIn(ExperimentalCoilApi::class)
    private fun startMainActivity(userValue: User) {
        User.setLoggedInUser(this, userValue)
        startActivity(Intent(this, MainActivity::class.java))
        finish()
    }
}
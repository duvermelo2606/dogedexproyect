package com.cursokotlin.dogedexproyect.auth

import android.content.Context
import android.os.Bundle
import android.util.Patterns
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.cursokotlin.dogedexproyect.R
import com.cursokotlin.dogedexproyect.databinding.FragmentSingUpBinding
import com.cursokotlin.dogedexproyect.isValidEmail

class SignUpFragment : Fragment() {

    interface SignUpFragmentActions {
        fun onSignUpFieldsValidated(email: String, password: String, passwordConfirmation: String)
    }

    private lateinit var signUpFragmentActions: SignUpFragmentActions

    override fun onAttach(context: Context) {
        super.onAttach(context)
        signUpFragmentActions = try {
            context as SignUpFragmentActions
        } catch (e: ClassCastException) {
            throw ClassCastException("$context must implement LoginFragmentActions")
        }
    }

    private lateinit var binding: FragmentSingUpBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentSingUpBinding.inflate(inflater)

        setupSignUpButton()

        return binding.root
    }

    private fun setupSignUpButton() {
        binding.btnSingUp.setOnClickListener {
            validateFields()
        }
    }

    private fun validateFields() {


        val email = binding.etEmail.text.toString()
        if (!isValidEmail(email)) {
            binding.etEmail.error = getString(R.string.email_is_not_valid)
            return
        }

        val password = binding.etPassword.text.toString()
        if (password.isEmpty()) {
            binding.etPassword.error = getString(R.string.password_must_not_be_empty)
            return
        }

        val passwordConfirmation = binding.etPasswordConfirm.text.toString()
        if (passwordConfirmation.isEmpty()) {
            binding.etPasswordConfirm.error = getString(R.string.password_must_not_be_empty)
            return
        }

        if (password != passwordConfirmation) {
            binding.etPassword.error = getString(R.string.password_do_not_match)
            return
        }

        signUpFragmentActions.onSignUpFieldsValidated(email, password, passwordConfirmation)

    }

}
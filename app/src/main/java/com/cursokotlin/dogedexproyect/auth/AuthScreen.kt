package com.cursokotlin.dogedexproyect.auth

import androidx.compose.runtime.Composable
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.cursokotlin.dogedexproyect.api.ApiResponseStatus
import com.cursokotlin.dogedexproyect.auth.AuthNavDestinations.LoginScreenDestination
import com.cursokotlin.dogedexproyect.auth.AuthNavDestinations.SignUpScreenDestination
import com.cursokotlin.dogedexproyect.composables.ErrorDialog
import com.cursokotlin.dogedexproyect.composables.LoadingWheel
import com.cursokotlin.dogedexproyect.model.User

@Composable
fun AuthScreen(
    onUserLoggedIn: (User) -> Unit,
    authViewModel: AuthViewModel = hiltViewModel(),
) {
    val user = authViewModel.user
    val userValue = user.value

    if (userValue != null) {
        onUserLoggedIn(userValue)
    }

    val navController = rememberNavController()
    val status = authViewModel.status.value

    AuthNavHost(
        navController,
        { email, password -> authViewModel.login(email, password) },
        { email, password, confirmPassword ->
            authViewModel.signUp(
                email,
                password,
                confirmPassword
            )
        },
        authViewModel,
        resetFieldErrors = {authViewModel.resetErrors()}
    )

    if (status is ApiResponseStatus.Loading) {
        LoadingWheel()
    } else if (status is ApiResponseStatus.Error) {
        ErrorDialog(
            messageId = status.messageId,
            onErrorDialogDismiss = { authViewModel.resetApiResponseStatus() })
    }
}

@Composable
fun AuthNavHost(
    navController: NavHostController,
    onLoginButtonClick: (String, String) -> Unit,
    onSignUpButtonClick: (email: String, password: String, confirmPassword: String) -> Unit,
    authViewModel: AuthViewModel,
    resetFieldErrors: () -> Unit
) {
    NavHost(navController = navController, startDestination = LoginScreenDestination) {
        composable(route = LoginScreenDestination) {
            LoginScreen(
                onLoginButtonClick = onLoginButtonClick,
                onRegisterButtonClick = { navController.navigate(route = SignUpScreenDestination) },
                authViewModel = authViewModel,
                resetFieldErrors = resetFieldErrors
            )
        }
        composable(route = SignUpScreenDestination) {
            SignUpScreen(
                onSignUpButtonClick = onSignUpButtonClick,
                // hace que  se devuelve a la screen anterior
                onNavigationIconClick = { navController.navigateUp() },
                authViewModel = authViewModel,
                resetFieldErrors = resetFieldErrors
            )
        }
    }
}

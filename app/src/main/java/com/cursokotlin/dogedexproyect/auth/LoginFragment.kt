package com.cursokotlin.dogedexproyect.auth

import android.content.Context
import android.os.Bundle
import android.util.Patterns
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.cursokotlin.dogedexproyect.R
import com.cursokotlin.dogedexproyect.databinding.FragmentLoginBinding
import com.cursokotlin.dogedexproyect.isValidEmail

class LoginFragment : Fragment() {

    interface LoginFragmentActions {
        fun onRegisterButtonClick()
        fun onLoginFieldsValidated(email: String, password: String)
    }

    private lateinit var loginFragmentActions: LoginFragmentActions
    private lateinit var binding: FragmentLoginBinding

    override fun onAttach(context: Context) {
        super.onAttach(context)
        loginFragmentActions = try {
            context as LoginFragmentActions
        } catch (e: ClassCastException) {
            throw ClassCastException("$context must implement LoginFragmentActions")
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentLoginBinding.inflate(inflater)

        binding.btnRegister.setOnClickListener {
            loginFragmentActions.onRegisterButtonClick()
        }
        binding.btnLogin.setOnClickListener {
            validateFields()
        }
        return binding.root
    }

    private fun validateFields() {
        val email = binding.etEmail.text.toString()

        if (!isValidEmail(email)) {
            binding.etEmail.error = getString(R.string.email_is_not_valid)
            return
        }

        val password = binding.etPassword.text.toString()
        if (password.isEmpty()) {
            binding.etPassword.error = getString(R.string.password_must_not_be_empty)
            return
        }

        loginFragmentActions.onLoginFieldsValidated(email, password)
    }
}
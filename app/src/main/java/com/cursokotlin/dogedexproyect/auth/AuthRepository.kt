package com.cursokotlin.dogedexproyect.auth

import com.cursokotlin.dogedexproyect.model.User
import com.cursokotlin.dogedexproyect.api.ApiResponseStatus
import com.cursokotlin.dogedexproyect.api.ApiService
import com.cursokotlin.dogedexproyect.api.DogsApi
import com.cursokotlin.dogedexproyect.api.dto.LoginDTO
import com.cursokotlin.dogedexproyect.api.dto.SignUpDTO
import com.cursokotlin.dogedexproyect.api.dto.UserDTOMapper
import com.cursokotlin.dogedexproyect.api.makeNetworkCall
import javax.inject.Inject

interface AuthTasks {
    suspend fun signUp(email: String, password: String, passwordConfirm: String): ApiResponseStatus<User>
    suspend fun login(email: String, password: String): ApiResponseStatus<User>
}

class AuthRepository @Inject constructor(
    private val apiService: ApiService
) : AuthTasks {
    override suspend fun signUp(email: String, password: String, passwordConfirm: String): ApiResponseStatus<User> = makeNetworkCall {
        val signUpDTO = SignUpDTO(email, password, passwordConfirm)
        val signUpResponse = apiService.signUp(signUpDTO)

        if (!signUpResponse.isSuccess) {
            throw Exception(signUpResponse.message)
        }

        val userDTO = signUpResponse.data.user
        val userDTOMapper = UserDTOMapper()
        userDTOMapper.fromUserDTOToUserDomain(userDTO)
    }

    override suspend fun login(email: String, password: String): ApiResponseStatus<User> = makeNetworkCall {
        val loginDTO = LoginDTO(email, password)
        val loginResponse = apiService.login(loginDTO)

        if (!loginResponse.isSuccess) {
            throw Exception(loginResponse.message)
        }

        val userDTO = loginResponse.data.user
        val userDTOMapper = UserDTOMapper()
        userDTOMapper.fromUserDTOToUserDomain(userDTO)
    }
}
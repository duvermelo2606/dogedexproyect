package com.cursokotlin.dogedexproyect.machinelearning

data class DogRecognition(val id: String, val confidence: Float)
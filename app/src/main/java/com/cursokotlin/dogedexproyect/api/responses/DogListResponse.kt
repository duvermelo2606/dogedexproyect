package com.cursokotlin.dogedexproyect.api.responses

import com.cursokotlin.dogedexproyect.api.dto.DogDTO

class DogListResponse(
    val dogs: List<DogDTO>
)
package com.cursokotlin.dogedexproyect.api.dto

import com.cursokotlin.dogedexproyect.model.User

class UserDTOMapper {
    fun fromUserDTOToUserDomain(userDTO: UserDTO): User = User(
        userDTO.id,
        userDTO.email,
        userDTO.authenticationToken
    )
}

package com.cursokotlin.dogedexproyect.api.responses

import com.cursokotlin.dogedexproyect.api.dto.UserDTO

class UserResponse (val user: UserDTO)
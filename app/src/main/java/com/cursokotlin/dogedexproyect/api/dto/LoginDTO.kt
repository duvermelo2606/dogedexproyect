package com.cursokotlin.dogedexproyect.api.dto

class LoginDTO(
    val email: String,
    val password: String
)
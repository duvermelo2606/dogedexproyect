package com.cursokotlin.dogedexproyect.api.responses

import com.cursokotlin.dogedexproyect.api.dto.DogDTO
import com.squareup.moshi.Json

class DogApiResponse(
    val message: String,
    @field:Json(name = "is_success") val isSuccess: Boolean,
    val data: DogResponse
)
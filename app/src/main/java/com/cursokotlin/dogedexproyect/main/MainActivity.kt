package com.cursokotlin.dogedexproyect.main

import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.viewModels
import androidx.appcompat.app.AlertDialog
import androidx.camera.core.*
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.core.content.ContextCompat
import coil.annotation.ExperimentalCoilApi
import com.cursokotlin.dogedexproyect.LABEL_PATH
import com.cursokotlin.dogedexproyect.MODEL_PATH
import com.cursokotlin.dogedexproyect.R
import com.cursokotlin.dogedexproyect.api.ApiResponseStatus
import com.cursokotlin.dogedexproyect.api.ApiServiceInterceptor
import com.cursokotlin.dogedexproyect.auth.LoginActivity
import com.cursokotlin.dogedexproyect.databinding.ActivityMainBinding
import com.cursokotlin.dogedexproyect.dogdetail.DogDetailComposeActivity
import com.cursokotlin.dogedexproyect.dogdetail.DogDetailComposeActivity.Companion.DOG_KEY
import com.cursokotlin.dogedexproyect.dogdetail.DogDetailComposeActivity.Companion.IS_RECOGNITION_KEY
import com.cursokotlin.dogedexproyect.dogdetail.DogDetailComposeActivity.Companion.MOST_PROBABLE_DOGS_IDS
import com.cursokotlin.dogedexproyect.doglist.DogListActivity
import com.cursokotlin.dogedexproyect.machinelearning.Classifier
import com.cursokotlin.dogedexproyect.machinelearning.DogRecognition
import com.cursokotlin.dogedexproyect.model.Dog
import com.cursokotlin.dogedexproyect.model.User
import com.cursokotlin.dogedexproyect.settings.SettingsActivity
import com.cursokotlin.dogedexproyect.testutils.EspressoIdLingResource
import dagger.hilt.android.AndroidEntryPoint
import org.tensorflow.lite.support.common.FileUtil
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

@ExperimentalCoilApi
@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private val requestPermissionLauncher =
        registerForActivityResult(
            ActivityResultContracts.RequestPermission()
        ) { isGranted: Boolean ->
            if (isGranted) {
                setUpCamera()
            } else {
                Toast.makeText(this, R.string.need_permission_camera, Toast.LENGTH_SHORT).show()
            }
        }

    private lateinit var binding: ActivityMainBinding
    private lateinit var imageCapture: ImageCapture
    private lateinit var cameraExecutor: ExecutorService
    private var isCameraReady = false
    private val viewModel: MainViewModel by viewModels()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val user = User.getLoggedInUser(this)
        if (user == null) {
            openLoginActivity()
            return
        } else {
            ApiServiceInterceptor.setSessionToken(user.authenticationToken)
        }

        binding.btnFloatingSettings.setOnClickListener {
            openSettingsActivity()
        }

        binding.btnFloatingDogList.setOnClickListener {
            openDogListActivity()
        }

        viewModel.status.observe(this) {status ->
            when(status) {
                is ApiResponseStatus.Error -> {
                    binding.pbLoadingWheel.visibility = View.GONE
                    Toast.makeText(this, status.messageId, Toast.LENGTH_SHORT).show()
                }
                is ApiResponseStatus.Loading -> binding.pbLoadingWheel.visibility = View.VISIBLE
            is ApiResponseStatus.Success -> binding.pbLoadingWheel.visibility = View.GONE
            }
        }

        viewModel.dog.observe(this) { dog ->
            if (dog != null) {
                openDogDetailActivity(dog)
            }
        }

        viewModel.dogRecognition.observe(this) { dogRecognition ->
            enabledTakePhotoButton(dogRecognition)
        }

        requestCameraPermission()
    }

    private fun openDogDetailActivity(dog: Dog) {
        val intent = Intent(this, DogDetailComposeActivity::class.java)
        intent.putExtra(DOG_KEY, dog)
        intent.putExtra(MOST_PROBABLE_DOGS_IDS, ArrayList<String>(viewModel.probableDogIds))
        intent.putExtra(IS_RECOGNITION_KEY, true)
        startActivity(intent)
    }

    private fun openDogListActivity() {
        startActivity(Intent(this, DogListActivity::class.java))
    }

    private fun openSettingsActivity() {
        startActivity(Intent(this, SettingsActivity::class.java))
    }

    private fun openLoginActivity() {
        startActivity(Intent(this, LoginActivity::class.java))
        finish()
    }

    private fun requestCameraPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            when {
                ContextCompat.checkSelfPermission(
                    this,
                    android.Manifest.permission.CAMERA
                ) == PackageManager.PERMISSION_GRANTED -> {
                    setUpCamera()
                }
                shouldShowRequestPermissionRationale(android.Manifest.permission.CAMERA) -> {
                    AlertDialog.Builder(this)
                        .setTitle("Acepta el permiso de la camera")
                        .setMessage("No podra tomarle fotos a perritos sin permiso")
                        .setPositiveButton(android.R.string.ok) { _, _ ->
                            requestPermissionLauncher.launch(
                                android.Manifest.permission.CAMERA
                            )
                        }
                        .setNegativeButton(android.R.string.cancel) { _, _ ->
                        }.show()

                }
                else -> {
                    requestPermissionLauncher.launch(
                        android.Manifest.permission.CAMERA
                    )
                }
            }
        } else {
            setUpCamera()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if (::cameraExecutor.isInitialized)
            cameraExecutor.shutdown()
    }

    private fun setUpCamera() {
        binding.previewCamera.post {
            imageCapture = ImageCapture.Builder()
                .setTargetRotation(binding.previewCamera.display.rotation)
                .build()
            cameraExecutor = Executors.newSingleThreadExecutor()
            isCameraReady = true
            startCamera()
        }
    }

    private fun startCamera() {
        val cameraProviderFuture = ProcessCameraProvider.getInstance(this)

        EspressoIdLingResource.increment()

        cameraProviderFuture.addListener({
            val cameraProvider = cameraProviderFuture.get()
            val preview = Preview.Builder().build()
            preview.setSurfaceProvider(binding.previewCamera.surfaceProvider)

            val cameraSelector = CameraSelector.DEFAULT_BACK_CAMERA

            val imageAnalysis = ImageAnalysis.Builder()
                .setBackpressureStrategy(ImageAnalysis.STRATEGY_KEEP_ONLY_LATEST)
                .build()
            imageAnalysis.setAnalyzer(cameraExecutor) { imageProxy ->
                EspressoIdLingResource.decrement()
                viewModel.recognizeImage(imageProxy)
            }


            cameraProvider.bindToLifecycle(
                this,
                cameraSelector,
                preview,
                imageCapture,
                imageAnalysis
            )
        }, ContextCompat.getMainExecutor(this))
    }

    private fun enabledTakePhotoButton(dogRecognition: DogRecognition) {
        if (dogRecognition.confidence > 70.0) {
            binding.btnFloatingTakePhoto.alpha = 1f
            binding.btnFloatingTakePhoto.setOnClickListener {
                viewModel.getDogByMLId(dogRecognition.id)
            }
        }else {
            binding.btnFloatingTakePhoto.alpha = 0.2f
            binding.btnFloatingTakePhoto.setOnClickListener(null)
        }
    }

    /*
           binding.btnFloatingTakePhoto.setOnClickListener {
            if (isCameraReady)
                takePhoto()
        }

    private fun takePhoto() {
        val outputFileOptions = ImageCapture.OutputFileOptions.Builder(getOutputPhotoFile()).build()
        imageCapture.takePicture(outputFileOptions, cameraExecutor,
            object : ImageCapture.OnImageSavedCallback {
                override fun onImageSaved(outputFileResults: ImageCapture.OutputFileResults) {

                }

                override fun onError(exception: ImageCaptureException) {
                    Toast.makeText(
                        this@MainActivity,
                        "Errortaking photo ${exception.message}",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            })
    }


    private fun getOutputPhotoFile(): File {
        val mediaDir = externalMediaDirs.firstOrNull()?.let {
            File(it, resources.getString(R.string.app_name) + ".jpg").apply { mkdirs() }
        }
        return if (mediaDir != null && mediaDir.exists()) {
            mediaDir
        } else {
            filesDir
        }
    }
    */

}
package com.cursokotlin.dogedexproyect.viewmodel

import com.cursokotlin.dogedexproyect.R
import com.cursokotlin.dogedexproyect.api.ApiResponseStatus
import com.cursokotlin.dogedexproyect.auth.AuthTasks
import com.cursokotlin.dogedexproyect.auth.AuthViewModel
import com.cursokotlin.dogedexproyect.model.User
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Rule
import org.junit.Test
import org.junit.Assert.*

class AuthViewModelTest {
    @ExperimentalCoroutinesApi
    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()

    @Test
    fun testLoginValidationIsCorrect() {
        class  FakeAuthRepository : AuthTasks {
            override suspend fun signUp(
                email: String,
                password: String,
                passwordConfirm: String
            ): ApiResponseStatus<User> {
                return  ApiResponseStatus.Success(
                    User(1, "", "")
                )
            }

            override suspend fun login(email: String, password: String): ApiResponseStatus<User> {
                return  ApiResponseStatus.Success(
                    User(2, "duver", "")
                )
            }

        }

        val viewModel = AuthViewModel(
            authRepository = FakeAuthRepository()
        )

        viewModel.login("", "password")
        assertEquals(R.string.email_is_not_valid, viewModel.emailError.value)
        viewModel.login("email", "")
        assertEquals(R.string.password_must_not_be_empty, viewModel.passwordError.value)
    }

    @Test
    fun testLoginStateIsCorrect() {

        val fakeUSer = User(1, "duver@gmail.com", "1234")

        class FakeAuthRepository: AuthTasks {
            override suspend fun signUp(
                email: String,
                password: String,
                passwordConfirm: String
            ): ApiResponseStatus<User> {
                return ApiResponseStatus.Success(fakeUSer)
            }

            override suspend fun login(email: String, password: String): ApiResponseStatus<User> {
                return ApiResponseStatus.Success(fakeUSer)
            }
        }

        val viewModel = AuthViewModel(
            authRepository = FakeAuthRepository()
        )

        viewModel.login("duver@gmail.com", "1234")
        assertEquals(fakeUSer.email, viewModel.user.value?.email)
    }
}
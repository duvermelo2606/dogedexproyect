package com.cursokotlin.dogedexproyect.viewmodel

import com.cursokotlin.dogedexproyect.api.ApiResponseStatus
import com.cursokotlin.dogedexproyect.doglist.DogListViewModel
import com.cursokotlin.dogedexproyect.doglist.DogTasks
import com.cursokotlin.dogedexproyect.model.Dog
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Rule
import org.junit.Test
import org.junit.Assert.*

class DogListViewModelTest {
    @ExperimentalCoroutinesApi
    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()

    @Test
    fun downloadDogListStatusIsCorrect() {

        class FakeDogRepository : DogTasks {
            override suspend fun getDogCollection(): ApiResponseStatus<List<Dog>> {
                return ApiResponseStatus.Success(
                    listOf(
                        Dog(1, 1, "", "", "", "", "", "", "", "", "", false),
                        Dog(10 , 2, "", "", "", "", "", "", "", "", "", false)
                    )
                )
            }

            override suspend fun addDogToUser(dogId: Long): ApiResponseStatus<Any> {
                return ApiResponseStatus.Success(Unit)
            }

            override suspend fun getDogByMLId(mlDogId: String): ApiResponseStatus<Dog> {
                return ApiResponseStatus.Success(
                    Dog(
                        1, 1, "", "", "", "", "", "", "", "", "", false
                    )
                )
            }
        }

        val viewModel = DogListViewModel(
            dogRepository = FakeDogRepository()
        )
        assertEquals(2, viewModel.dogList.value.size)
        assert(viewModel.status.value is ApiResponseStatus.Success)
        assertEquals(10, viewModel.dogList.value[1].id)
    }

    @Test
    fun downloadDogListErrorStatusIsCorrect() {

        class FakeDogRepository : DogTasks {
            override suspend fun getDogCollection(): ApiResponseStatus<List<Dog>> {
                return ApiResponseStatus.Error(messageId = 12)
            }

            override suspend fun addDogToUser(dogId: Long): ApiResponseStatus<Any> {
                return ApiResponseStatus.Success(Unit)
            }

            override suspend fun getDogByMLId(mlDogId: String): ApiResponseStatus<Dog> {
                return ApiResponseStatus.Success(
                    Dog(
                        1, 1, "", "", "", "", "", "", "", "", "", false
                    )
                )
            }
        }

        val viewModel = DogListViewModel(
            dogRepository = FakeDogRepository()
        )
        assertEquals(0, viewModel.dogList.value.size)
        assert(viewModel.status.value is ApiResponseStatus.Error)
    }

    @Test
    fun resetStatusIsCorrect() {

        class FakeDogRepository : DogTasks {
            override suspend fun getDogCollection(): ApiResponseStatus<List<Dog>> {
                return ApiResponseStatus.Error(messageId = 12)
            }

            override suspend fun addDogToUser(dogId: Long): ApiResponseStatus<Any> {
                return ApiResponseStatus.Success(Unit)
            }

            override suspend fun getDogByMLId(mlDogId: String): ApiResponseStatus<Dog> {
                return ApiResponseStatus.Success(
                    Dog(
                        1, 1, "", "", "", "", "", "", "", "", "", false
                    )
                )
            }
        }

        val viewModel = DogListViewModel(
            dogRepository = FakeDogRepository()
        )
        assertEquals(0, viewModel.dogList.value.size)
        viewModel.resetApiResponseStatus()
        assert(viewModel.status.value == null)
    }

}
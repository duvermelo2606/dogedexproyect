package com.cursokotlin.dogedexproyect

import com.cursokotlin.dogedexproyect.api.ApiResponseStatus
import com.cursokotlin.dogedexproyect.api.ApiService
import com.cursokotlin.dogedexproyect.api.dto.AddDogToUserDTO
import com.cursokotlin.dogedexproyect.api.dto.DogDTO
import com.cursokotlin.dogedexproyect.api.dto.LoginDTO
import com.cursokotlin.dogedexproyect.api.dto.SignUpDTO
import com.cursokotlin.dogedexproyect.api.responses.*
import com.cursokotlin.dogedexproyect.doglist.DogRepository
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.TestCoroutineDispatcher
import org.junit.Test
import org.junit.Assert.*
import java.net.UnknownHostException

class DogRepositoryTest {
    @Test
    fun testGetDogCollectionSuccess(): Unit = runBlocking {

        class FakeApiService : ApiService {
            override suspend fun getAllDogs(): DogListApiResponse {
                return DogListApiResponse(
                    message = "",
                    isSuccess = true,
                    data = DogListResponse(
                        listOf(
                            DogDTO(1, 1, "", "", "", "", "", "", "", "", ""),
                            DogDTO(10, 2, "Koby", "", "", "", "", "", "", "", "")
                        )
                    )
                )
            }

            override suspend fun signUp(signUpDTO: SignUpDTO): AuthApiResponse {
                TODO("Not yet implemented")
            }

            override suspend fun login(loginDTO: LoginDTO): AuthApiResponse {
                TODO("Not yet implemented")
            }

            override suspend fun addDogToUser(addDogToUserDTO: AddDogToUserDTO): DefaultResponse {
                TODO("Not yet implemented")
            }

            override suspend fun getUserDogs(): DogListApiResponse {
                return DogListApiResponse(
                    message = "",
                    isSuccess = true,
                    data = DogListResponse(
                        listOf(
                            DogDTO(10, 2, "Koby", "", "", "", "", "", "", "", "")
                        )
                    )
                )
            }

            override suspend fun getDogByMLId(mlId: String): DogApiResponse {
                TODO("Not yet implemented")
            }

        }

        val dogRepository = DogRepository(
            apiService = FakeApiService(),
            dispatcher = TestCoroutineDispatcher()
        )

        val apiResponseStatus = dogRepository.getDogCollection()
        assert(apiResponseStatus is ApiResponseStatus.Success)
        val dogCollection = (apiResponseStatus as ApiResponseStatus.Success).data
        assertEquals(2, dogCollection.size)
        assertEquals("Koby", dogCollection[1].name)
    }

    @Test
    fun testGetAllDogsError(): Unit = runBlocking {

        class FakeApiService : ApiService {
            override suspend fun getAllDogs(): DogListApiResponse {
                throw UnknownHostException()
            }

            override suspend fun signUp(signUpDTO: SignUpDTO): AuthApiResponse {
                TODO("Not yet implemented")
            }

            override suspend fun login(loginDTO: LoginDTO): AuthApiResponse {
                TODO("Not yet implemented")
            }

            override suspend fun addDogToUser(addDogToUserDTO: AddDogToUserDTO): DefaultResponse {
                TODO("Not yet implemented")
            }

            override suspend fun getUserDogs(): DogListApiResponse {
                return DogListApiResponse(
                    message = "",
                    isSuccess = true,
                    data = DogListResponse(
                        listOf(
                            DogDTO(10, 2, "Koby", "", "", "", "", "", "", "", "")
                        )
                    )
                )
            }

            override suspend fun getDogByMLId(mlId: String): DogApiResponse {
                TODO("Not yet implemented")
            }

        }

        val dogRepository = DogRepository(
            apiService = FakeApiService(),
            dispatcher = TestCoroutineDispatcher()
        )

        val apiResponseStatus = dogRepository.getDogCollection()
        assert(apiResponseStatus is ApiResponseStatus.Error)
        assertEquals(
            R.string.unknow_host_exception_error,
            (apiResponseStatus as ApiResponseStatus.Error).messageId
        )
    }

    @Test
    fun getDogByMachineLearningIsCorrect() = runBlocking {
        class FakeApiService : ApiService {
            override suspend fun getAllDogs(): DogListApiResponse {
                TODO("Not yet implemented")
            }

            override suspend fun signUp(signUpDTO: SignUpDTO): AuthApiResponse {
                TODO("Not yet implemented")
            }

            override suspend fun login(loginDTO: LoginDTO): AuthApiResponse {
                TODO("Not yet implemented")
            }

            override suspend fun addDogToUser(addDogToUserDTO: AddDogToUserDTO): DefaultResponse {
                TODO("Not yet implemented")
            }

            override suspend fun getUserDogs(): DogListApiResponse {
                TODO("Not yet implemented")
            }

            override suspend fun getDogByMLId(mlId: String): DogApiResponse {
                return DogApiResponse(
                    message = "",
                    isSuccess = true,
                    data = DogResponse(
                        dog = DogDTO(10, 2, "Koby", "", "", "", "", "", "", "", "")
                    )
                )
            }
        }
        val dogRepository =DogRepository(
            apiService = FakeApiService(),
            dispatcher = TestCoroutineDispatcher()
        )

        val apiResponseStatus = dogRepository.getDogByMLId("test")
        assert(apiResponseStatus is ApiResponseStatus.Success)
        assertEquals(10, (apiResponseStatus as ApiResponseStatus.Success).data.id)
    }

    @Test
    fun getDogByMachineLearningError() = runBlocking {
        class FakeApiService : ApiService {
            override suspend fun getAllDogs(): DogListApiResponse {
                TODO("Not yet implemented")
            }

            override suspend fun signUp(signUpDTO: SignUpDTO): AuthApiResponse {
                TODO("Not yet implemented")
            }

            override suspend fun login(loginDTO: LoginDTO): AuthApiResponse {
                TODO("Not yet implemented")
            }

            override suspend fun addDogToUser(addDogToUserDTO: AddDogToUserDTO): DefaultResponse {
                TODO("Not yet implemented")
            }

            override suspend fun getUserDogs(): DogListApiResponse {
                TODO("Not yet implemented")
            }

            override suspend fun getDogByMLId(mlId: String): DogApiResponse {
                return DogApiResponse(
                    message = "Error_getting_dog_by_machine_learning_id",
                    isSuccess = false,
                    data = DogResponse(
                        dog = DogDTO(10, 2, "Koby", "", "", "", "", "", "", "", "")
                    )
                )
            }
        }
        val dogRepository =DogRepository(
            apiService = FakeApiService(),
            dispatcher = TestCoroutineDispatcher()
        )

        val apiResponseStatus = dogRepository.getDogByMLId("test")
        assert(apiResponseStatus is ApiResponseStatus.Error)
        assertEquals(R.string.unknow_error, (apiResponseStatus as ApiResponseStatus.Error).messageId)
    }
}